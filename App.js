import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import Router from './src/router/index'
import { Provider } from 'react-redux';
import { store, Persistor } from './src/redux/store';
import { PersistGate } from 'redux-persist/integration/react';
// import { NavigationContainer } from '@react-navigation/native';
// import NetInfo from '@react-native-community/netinfo';

const App = () => {
  return (

    <Provider store={store}>
      <PersistGate loading={null} persistor={Persistor}>
        <NavigationContainer>
          <Router />
        </NavigationContainer>
      </PersistGate>
    </Provider>

  )
}

export default App

const styles = StyleSheet.create({})