import 'react-native';
import React from 'react';
import HomeScreen from '../../src/pages/HomeScreen';
import renderer from 'react-test-renderer';


it('function and state test care', ()=>{
    let HomeData = renderer.create(<HomeScreen />).getInstance();

    HomeData.change(2)

    hasExpectedRequestMetadata(HomeData.state.data).toEqual(10)
}) 