import 'react-native';
import React from 'react';
import HomeScreen from '../../src/pages/HomeScreen';
import renderer from 'react-test-renderer';


test('Home Snapshot', () => {
    const snap = renderer.create(
            <HomeScreen />
    ).toJSON();
    expect(snap).toMatchSnapshot()
})