import 'react-native';
import React from 'react';
import LoginScreen from '../../src/pages/LoginScreen';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import {store} from '../../src/redux/store';
import AsyncStorage from '@react-native-community/async-storage';

test('Login Snapshot', () => {
    const snap = renderer.create(
        <Provider store={store}>
            <LoginScreen />
        </Provider>
    ).toJSON();
    expect(snap).toMatchSnapshot()
})