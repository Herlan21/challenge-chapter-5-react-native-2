import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
// import dataLogin from '../../src/test/dataLogin'
import {Login} from '../../src/test/loginApi'


describe('Api', () => {
    it('Api Login', async () => {
      let mockApi = new MockAdapter(axios);
      const loginBody = {
        email: 'basreng@yahoo.com',
        password: 'basreng1234',
      };
  
      mockApi
        .onPost('http://code.aldipee.com/api/v1/auth/login')
        .reply(400, []); //diganti array kosong [dataLogin]
  
      // act
      let res = await Login(loginBody);
    
      expect(res.data).toEqual(); //dihilangkan
      expect(res.status).toEqual(400); //reply 400 yg 200
    });
  });