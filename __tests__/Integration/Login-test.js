import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import dataLogin from '../../src/test/dataLogin'
import {Login} from '../../src/test/loginApi'


describe('Api', () => {
    it('Api Login', async () => {
      let mockApi = new MockAdapter(axios);
      const loginBody = {
        email: 'basreng@yahoo.com',
        password: 'basreng123',
      };
  
      mockApi
        .onPost('http://code.aldipee.com/api/v1/auth/login')
        .reply(200, dataLogin); //diganti array kosong [dataLogin]
  
      // act
      let res = await Login(loginBody);
      
      expect(res.data).toEqual(dataLogin); //dihilangkan
      expect(res.status).toEqual(200); //reply 400 yg 200
    });
  });