import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import dataBooks from '../../src/test/dataHome'
import {HomeApi} from '../../src/test/homeApi'


describe('Api', () => {
    it('Api Home', async () => {
      let mockApi = new MockAdapter(axios);
      const homeBody = {
        email: 'basreng@yahoo.com',
        password: 'basreng123'
      };
  
      mockApi
        .onGet('http://code.aldipee.com/api/v1/books')
        .reply(200, dataBooks); //diganti array kosong [dataLogin]
  
      // act
      let response = await HomeApi(homeBody);
      console.log(response.data);
      expect(response.data).toEqual(dataBooks); //dihilangkan
      expect(response.status).toEqual(200); //reply 400 yg 200
    });
  });