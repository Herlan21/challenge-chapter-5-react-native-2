import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import dataRegister from '../../src/test/dataRegister'
import {Register} from '../../src/test/registerApi'


describe('Api', () => {
    it('Api Register', async () => {
      let mockApi = new MockAdapter(axios);
      const registerBody = {
        email: 'basreng@yahoo.com',
        password: 'basreng123',
        name: 'basreng'
      };
  
      mockApi
        .onPost('http://code.aldipee.com/api/v1/auth/register')
        .reply(200, dataRegister);
  
      // act
      let res = await Register(registerBody);
      
      expect(res.data).toEqual(dataRegister);
      expect(res.status).toEqual(200);
    });
  });