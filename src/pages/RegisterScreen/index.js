import { StyleSheet, Text, View, TouchableOpacity, Dimensions, Alert, TextInput } from 'react-native'
import React, { useState } from 'react'
import { RegisterButton } from '../../components'
import { useDispatch, useSelector } from 'react-redux'
import { registerSuccess } from '../../redux/actions'

const RegisterScreen = ({navigation}) => {

  const dispatch = useDispatch()

  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const isRegister = useSelector(state => {
    console.log(state.appData.isRegister);
    return state.appData.isRegister
  })

  function kirimData() {
    const sendData = {
      email: email,
      password: password,
      name: name
    }
    console.log('form', sendData);
    dispatch(registerSuccess(sendData))
    isRegister ? navigation.navigate('Success') : Alert.alert('Register Failed')
  }

  return (
    <View style={styles.container}>

      <View style={styles.wrapper}>
        <Text style={styles.title}>Sign Up for account</Text>

        <View>
          {/* <Register/> */}
          <Text style={{ fontSize: 14, color: '#000', fontWeight: '600', marginBottom: 8 }}>Name</Text>
          <TextInput
            style={styles.textinput}
            placeholder="Name"
            placeholderTextColor="#bbb"
            onChangeText={text => setName(text)}
            value={name}
          />

          <Text style={{ fontSize: 14, color: '#000', fontWeight: '600', marginBottom: 8 }}>Email</Text>
          <TextInput
            style={styles.textinput}
            placeholder="Email"
            placeholderTextColor="#bbb"
            onChangeText={text => setEmail(text)}
            value={email}
          />

          <Text style={{ fontSize: 14, color: '#000', fontWeight: '600', marginBottom: 8 }}>Password</Text>
          <TextInput
            style={styles.textinput}
            placeholderTextColor="#bbb"
            placeholder="Password"
            secureTextEntry
            onChangeText={text => setPassword(text)}
            value={password}
          />


          <RegisterButton
            onPress={kirimData}
          />


          <View style={styles.register}>
            <Text style={{ color: '#000' }}>have an account? </Text>

            <TouchableOpacity
              onPress={() => navigation.navigate('Login')}>

              <Text style={{ color: '#1b30d1', fontWeight: 'bold' }}>Login here</Text>
            </TouchableOpacity>
          </View>

        </View>
      </View>
    </View>
  )
}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default RegisterScreen

const styles = StyleSheet.create({

  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#8591d6',
    margin: 10,
    textAlign: 'center',
    top: 20
  },

  text: {
    color: 'gray',
    margin: 20,
    textAlign: 'center',
    top: 20,
  },

  SignUp: {
    color: '#FFF',
    backgroundColor: '#8591d6',
    textAlign: 'center',
    width: 100,
    height: 35,
    borderRadius: 8,
    fontSize: 22,
    fontWeight: 'bold'

  },

  SignUpButton: {
    alignItems: 'center',
    top: 20
  },

  registerScreen: {
    height: windowHeight,
    backgroundColor: '#ffffff',
  },

  register: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'center'
  },

  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#8591d6',
    margin: 10,
    textAlign: 'center',
    top: -20
  },

  wrapper: {
    width: '80%'
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  register: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'center'
  },

  textinput: {
    borderWidth: 0.95,
    borderColor: '#bbb',
    marginBottom: 12,
    borderRadius: 8,
    color: '#000',
    paddingHorizontal: 15
  },
})