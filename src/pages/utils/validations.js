import validator from 'is_js';

const checkEmpty = (value, key) => {
    if (validator.empty(value.trim())) {
        return `${key}`;
    } else {
        return '';
    }
}

const checkMinLength = (value, minLength, key) => {
    if (value.trim().length < minLength) {
        return `Please enter a valid ${key}`
    } else {
        return '';
    }
}
 
export default function (data) {
    
    const { name, email, password } = data

    if (name !== undefined) {
        let emptyValidationText = checkEmpty(name, 'Please enter your username')
        if (emptyValidationText !== '') {
            return emptyValidationText;
        } else {
            let minLengthValidation = checkMinLength(name, 3, 'name')
            if (minLengthValidation !=='') {
                return minLengthValidation
            }
        }
    }

    if (email !== undefined) {
        let emptyValidationText = checkEmpty(email, 'Please enter your email')
        if (emptyValidationText !== '') {
            return emptyValidationText;
        } else {
            if (!validator.email(email)) {
                return 'Please enter a valid email'
            }
        }
    }

    if (password !== undefined) {
        let emptyValidationText = checkEmpty(password, 'Please enter your password')
        if (emptyValidationText !== '') {
            return emptyValidationText;
        } else {
            let minLengthValidation = checkMinLength(password, 6, 'password')
            if (minLengthValidation !== '') {
                return minLengthValidation
            }
        }
    }
}


