import { StyleSheet, Text, View, Touchableopacity, Image, ScrollView } from 'react-native';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { homeSuccess } from '../../redux/actions';
import { RecommendedBook, PopularBook } from '../../components';
import Pdf from 'react-native-pdf'

const HomeScreen = () => {

  const bookData = useSelector(state => {
    console.log('databuku', state.appData.bookData);
    return state.appData.bookData
  })

  const isLoading = useSelector(state => {
    console.log('databuku', state.appData.loading);
    return state.appData.loading
  })

  const token = useSelector(state => {
    console.log('token', state.appData.userData.tokens.access.token);
    return state.appData.userData
  })


  useEffect(() => {
    dispatch(homeSuccess(token.tokens.access.token))
  }, [])

  if (!isLoading) {



    return (


      <ScrollView>
        <View>

          {/* //! RECOMENDED */}
          <Text style={styles.textRecommended}>Recommended</Text>

          <ScrollView horizontal={true}>
            <View>
              <RecommendedBook data={bookData} />
            </View>
          </ScrollView>

          {/*//! PoPularBook */}
          <Text style={styles.textRecommended}>Popular Book</Text>
          <ScrollView horizontal={true}>
            <PopularBook data={bookData} />

             {/* //! PDF */}
            <View style={styles.container}>
              <Text style={styles.pdf}>My Document Project</Text>
            </View>

            <Button
              onPress={onButtonOpenClick}
              title='Open PDF Document' />

            <Pdf
              source={{ uri: pdf }}
              style={styles.document} />
          </ScrollView>
        </View>
      </ScrollView>

    )
  } else {
    return <Text style={styles.textRecommended}>....... Loading ......</Text>
  }
}

export default HomeScreen

const styles = StyleSheet.create({

  textRecommended: {
    fontSize: 20,
    marginTop: 15,
    marginLeft: 25,
    fontWeight: 'bold',
    color: '#8591d6',
  }
})