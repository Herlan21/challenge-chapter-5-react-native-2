import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Alert } from 'react-native';
import { LoginButton, Login } from '../../components';
import { useDispatch, useSelector } from 'react-redux';
import { loginSuccess } from '../../redux/actions';

const LoginScreen = ({ navigation }) => {

  const dispatch = useDispatch();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const isLogin = useSelector(state =>{
    return state.appData.isLogin
  })

  function dataUser() {
    const data = {
      email: email,
      password: password,
    };
    console.log(data);
    dispatch(loginSuccess(data))
    isLogin ? navigation.navigate('Home') : Alert.alert('Login Gagal');
  }

  return (
    <View style={styles.container}>

      <View style={styles.wrapper}>
        <Text style={styles.title}>Sign In to your account</Text>

        <View>

          <Text style={{ fontSize: 14, color: '#000', fontWeight: '600', marginBottom: 8 }}>Email</Text>
          <TextInput
            style={styles.textinput}
            placeholder="Email"
            value={email}
            onChangeText={value => setEmail(value)}
            placeholderTextColor="#bbb"

          />

          <Text style={{ fontSize: 14, color: '#000', fontWeight: '600', marginBottom: 8 }}>Password</Text>
          <TextInput
            style={styles.textinput}
            placeholderTextColor="#bbb"
            placeholder="Password"
            value={password}
            secureTextEntry={true}
            onChangeText={value => setPassword(value)}

          />

          <LoginButton
            onPress={dataUser}
          />

          <View style={styles.register}>
            <Text style={{ color: '#000' }}>Don't have an account? </Text>

            <TouchableOpacity
              onPress={() => navigation.navigate('Register')}>

              <Text style={{ color: '#1b30d1', fontWeight: 'bold' }}>Register here</Text>
            </TouchableOpacity>
          </View>

        </View>
      </View>
    </View>
  )
}



export default LoginScreen

const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#8591d6',
    margin: 10,
    textAlign: 'center',
    top: -20
  },

  wrapper: {
    width: '80%'
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  register: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'center'
  },

  textinput: {
    borderWidth: 0.95,
    borderColor: '#bbb',
    marginBottom: 12,
    borderRadius: 8,
    color: '#000',
    paddingHorizontal: 15
  },
})