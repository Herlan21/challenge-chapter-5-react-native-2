import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import LottieView from 'lottie-react-native';

const SuccessScreen = ({ navigation }) => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text style={styles.completed}>Registration Completed !</Text>
      <LottieView style={{ width: 250 }} source={require('../../assets/success.json')} autoPlay loop />
      <Text style={styles.verification}>We sent email verification to your email</Text>

      <TouchableOpacity
        onPress={() => navigation.replace('Login')}>

        <Text style={{ color: '#1b30d1', fontWeight: 'bold' }}>Back To Login</Text>
      </TouchableOpacity>
    </View>
  )
}

export default SuccessScreen

const styles = StyleSheet.create({

  completed: {
    color: '#000',
    fontSize: 25,
    top: -100
  },

  verification: {
    color: '#000',
    fontSize: 15,
    marginTop: 10
  }
})