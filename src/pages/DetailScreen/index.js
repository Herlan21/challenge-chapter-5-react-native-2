import { StyleSheet, Text, View, Image, Dimensions, ScrollView, TouchableOpacity } from 'react-native'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { detailBook } from '../../redux/actions'

const DetailBook = ({ route, navigation }) => {

  const { id } = route.params
  console.log('aweu', id);

  const dispatch = useDispatch()

  const detailData = useSelector(state => {
    console.log('databuku', state.appData.detailData);
    return state.appData.detailData
  })

  const token = useSelector(state => {
    console.log('token', state.appData.userData.tokens.access.token);
    return state.appData.userData
  })


  useEffect(() => {
    dispatch(detailBook(token.tokens.access.token, id))
  }, [])


  return (
    <ScrollView>
      <View styles={{ backgroundColor: '#bbb' }}>
        <View style={styles.background}>

          <View style={styles.containerDetail}>
            <Text style={styles.namaBukuTitle}>{detailData.title}</Text>
            <Text style={styles.namaBuku}>{detailData.author}</Text>
            <Text style={styles.namaBuku}>{detailData.publisher}</Text>
          </View>

          <View style={styles.containerGambar}>
            <Image
              style={styles.gambarBuku}
              source={{ uri: detailData.cover_image }}
            />
          </View>
        </View>


      {/* //! RATING,TOTAL SALE */}
        <View style={{ alignItems: 'center' }}>
          <View style={styles.kartuRating}>
            <Text style={styles.rating}>Rating {"\n"} {detailData.average_rating}</Text>
            <Text style={styles.rating}> Total Sale {"\n"} {detailData.total_sale}</Text>

            <TouchableOpacity>
              <Text style={styles.login}>Buy now</Text>
            </TouchableOpacity>
          </View>
        </View>

        {/* //! OVERVIEW */}
        <Text style={styles.overview}>Overview</Text>
        <View style={styles.containerOverview}>
          <Text style={styles.synopsis}>{detailData.synopsis}</Text>
        </View>
      </View>
    </ScrollView>

  )
}
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default DetailBook

const styles = StyleSheet.create({

  background: {
    shadowColor: "#bbb",
    backgroundColor: '#fff',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.00,
    elevation: 1,

    borderRadius: 10,
    marginTop: 65,
    width: 400,
    height: 290,
    marginLeft: 6

  },

  containerGambar: {
    marginTop: 20
  },

  gambarBuku: {
    width: 150,
    height: 250,
    marginLeft: 18,
    marginTop: -155,
    borderRadius: 10,

  },

  namaBuku: {
    color: '#000',
    width: 150,
  },

  namaBukuTitle: {
    color: '#000',
    width: 150,
    fontWeight: 'bold',
    paddingLeft: -20,

  },

  containerDetail: {
    width: windowWidth,
    justifyContent: 'flex-end',
    marginTop: 80,
    marginLeft: 180
  },

  textRecommended: {
    fontSize: 20,
    marginTop: 15,
    marginLeft: 25,
    fontWeight: 'bold',
    color: '#8591d6',
  },

  rating: {
    color: '#000',
    fontWeight: 'bold',
    fontSize: 18,
    marginRight: 20,
    textAlign: 'center',
  },

  kartuRating: {
    borderRadius: 10,
    marginTop: 15,
    width: 395,
    height: 90,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    

    shadowColor: "#bbb",
    backgroundColor: '#fff',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.00,

    elevation: 1,
  },

  login: {
    color: '#FFF',
    backgroundColor: '#8591d6',
    textAlign: 'center',
    width: 100,
    height: 35,
    borderRadius: 8,
    fontSize: 21,
    fontWeight: 'bold'
  },

  LoginButton: {
    alignItems: 'center',
    top: 5
  },

  judulrating: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 15
  },

  overview: {
    fontSize: 20,
    marginTop: 15,
    marginLeft: 25,
    fontWeight: 'bold',
    color: '#8591d6',
  },

  synopsis: {
    color: '#000',

  },

  containerOverview: {
    borderRadius: 10,
    marginTop: 15,
    width: 390,
    left: 10,
    // height: 200,
  },
})