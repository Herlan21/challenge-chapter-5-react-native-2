import axios from "axios";

export const Register = (data) => {
  try {
    const response = axios.post('http://code.aldipee.com/api/v1/auth/register', data);
    return response
    
  } catch (error) {
    console.log(error);
  }
}