export const homeDummyData = {
    "user": {
        "role": "user",
        "isEmailVerified": true,
        "email": "basreng@yahoo.com",
        "name": "Basreng",
        "id": "6251b45898b5e97fae1710c2"
    },
    "tokens": {
        "access": {
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjYxYzk4MjVjNmFjMjJmY2I4MDI5NWYiLCJpYXQiOjE2NTA1ODU3OTUsImV4cCI6MTY1MDU4NzU5NSwidHlwZSI6ImFjY2VzcyJ9.b5fqokYaxN7lVfvl7kVwBh83pzk0oG_-29CBxWLSaes",
            "expires": "2022-04-22T00:33:15.082Z"
        },
        "refresh": {
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjYxYzk4MjVjNmFjMjJmY2I4MDI5NWYiLCJpYXQiOjE2NTA1ODU3OTUsImV4cCI6MTY1MzE3Nzc5NSwidHlwZSI6InJlZnJlc2gifQ.Q6dRtv2s_QDRvBmRfnNkgDZ4dmeIKuMf_lMq7ZIl5MY",
            "expires": "2022-05-22T00:03:15.083Z"
        }
    }
}