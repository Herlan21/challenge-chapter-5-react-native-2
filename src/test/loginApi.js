import axios from "axios";

export const Login = (data) => {
  try {
    const response = axios.post('http://code.aldipee.com/api/v1/auth/login', data);
    return response
    
  } catch (error) {
    console.log(error);
  }
}