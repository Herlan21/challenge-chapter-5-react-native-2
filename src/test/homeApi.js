import axios from "axios";

export const HomeApi = (data) => {
  try {
    const response = axios.get('http://code.aldipee.com/api/v1/books', data);
    return response
    
  } catch (error) {
    console.log(error);
  }
}