import { REGISTER_SUCCESS, LOGIN_SUCCESS, HOME_SUCCESS, DETAIL_BOOK, LOADING } from './types';

const initialState = {
    userData: [],
    bookData: [],
    detailData: [],
    loading: false,
    isRegister: false,
    isLogin: false,
}

const Reducers = (state = initialState, action) => {
    switch (action.type) {
        case REGISTER_SUCCESS:
            return {
                ...state,
                isRegister: true,
            }

        case LOGIN_SUCCESS:
            return {
                ...state,
                userData: action.payload,
                isLogin: true
            }

        case HOME_SUCCESS:
            return {
                ...state,
                bookData: action.payload,
            }

        case DETAIL_BOOK:
            return {
                ...state,
                detailData: action.payload,
            }

        case LOADING:
            return {
                ...state,
                loading: action.payload
            }

        default:
            return state

    }
}

export default Reducers