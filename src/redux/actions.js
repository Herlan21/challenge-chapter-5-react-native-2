import { REGISTER_SUCCESS, LOGIN_SUCCESS, HOME_SUCCESS, DETAIL_BOOK, LOADING } from './types';
import axios from "axios";
import { Alert } from 'react-native';

export const RegisterSuccess = payload => ({
  type: REGISTER_SUCCESS,
  payload: payload
});

export const LoginSuccess = payload => ({
  type: LOGIN_SUCCESS,
  payload: payload
});

export const HomeSuccess = payload => ({
  type: HOME_SUCCESS,
  payload: payload
});

export const DetailBook = payload => ({
  type: DETAIL_BOOK,
  payload: payload
});

export const Loading = payload => ({
  type: LOADING,
  payload: payload
});


//! REGISTER
export const registerSuccess = data =>
  async dispatch => {
    try {
      await axios.post('http://code.aldipee.com/api/v1/auth/register', data)
        .then(response => {
          dispatch(RegisterSuccess(response))
          Alert.alert('Register Success')
        })
    } catch (error) {
      console.log(error);
    }
  };


//! LOGIN
export const loginSuccess = data => async dispatch => {
  dispatch(Loading(true));
  try {
    await axios.post('http://code.aldipee.com/api/v1/auth/login', data)
      .then(response => {
        console.log('Login Success');
        dispatch(LoginSuccess(response.data));
        dispatch(Loading(false));
      });
  } catch (error) {
    dispatch(Loading(false));
    console.log(error);
  }
};

//! HOME
export const homeSuccess = token => async dispatch => {
  // dispatch(isLoading(true));

  try {
    await axios.get('http://code.aldipee.com/api/v1/books', {
      headers: {
        Authorization: 'Bearer ' + token, 
      }
    })
      .then(response => {
        console.log(response);
        dispatch(HomeSuccess(response.data.results));
        // dispatch(isLoading(false));
      });
  } catch (error) {
    // dispatch(isLoading(false));
    console.log(error);
  }
};

//! DETAIL BOOK
export const detailBook = (token, id) => async dispatch => {
  // dispatch(isLoading(true));

  try {
    await axios.get(`http://code.aldipee.com/api/v1/books/${id}`, {
      headers: {
        Authorization: 'Bearer ' + token, 
      }
    })
      .then(response => {
        console.log(response);
        dispatch(DetailBook(response.data));
        // dispatch(isLoading(false));
      });
  } catch (error) {
    // dispatch(isLoading(false));
    console.log(error);
  }
};

// detailbook(token, id)