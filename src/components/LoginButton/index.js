import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'


const LoginButton = ({ onPress, onChangeText }) => {

    return (
        <View style={styles.LoginButton}>
            <TouchableOpacity
                onPress={onPress}>
                
                <Text style={styles.login}>Login</Text>
            </TouchableOpacity>
        </View>
    )
}

export default LoginButton

const styles = StyleSheet.create({

    login: {
        color: '#FFF',
        backgroundColor: '#8591d6',
        textAlign: 'center',
        width: 100,
        height: 35,
        borderRadius: 8,
        fontSize: 21,
        fontWeight: 'bold'
    },

    LoginButton: {
        alignItems: 'center',
        top: 5
    },
})