import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import { useNavigation } from '@react-navigation/native'

const RecommendedBook = (bookData) => {

    const navigation = useNavigation()
    
    return (
        <View style={styles.container}>

            {/* PENTING */}
            {bookData.data?.map(getBuku => (

                <View>
                    <TouchableOpacity style={{ width: 150, marginRight: 20 }} onPress={() => navigation.navigate('Detail', {id:getBuku.id})}>

                        <Image style={styles.posterRecommended}
                            source={{ uri: getBuku.cover_image }}
                        />

                        <View>
                            <Text style={styles.keterangan} numberOfLines={1} ellipsizeMode='tail' >{getBuku.title}</Text>
                            <Text style={styles.keterangan} numberOfLines={1} ellipsizeMode='tail' >{getBuku.average_rating}</Text>
                            <Text style={styles.keterangan} numberOfLines={1} ellipsizeMode='tail' >{getBuku.author}</Text>
                            <Text style={styles.keterangan} numberOfLines={1} ellipsizeMode='tail' >{getBuku.publisher}</Text>
                            <Text style={styles.keterangan} numberOfLines={1} ellipsizeMode='tail' >Rp.{getBuku.price}</Text>
                        </View>

                    </TouchableOpacity>
                </View>
            ))}
        </View>
    )
}

export default RecommendedBook

const styles = StyleSheet.create({

    container: {
        flexDirection: 'row',
    },

    posterRecommended: {
        width: 150,
        height: 250,
        marginTop: 15,
        marginLeft: 10,
        marginRight: 10,
    },

    keterangan: {
        color: '#000',
        marginLeft: 10
        
    },
})